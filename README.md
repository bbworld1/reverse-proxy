# reverse-proxy

This is a tiny host/path reverse proxy built to learn Go. It's all stdlib
and should be able to serve about 10k requests per second.

## Usage

`go run . test.conf 8000`
