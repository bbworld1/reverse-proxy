package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	"os"
)

type Config struct {
    Routes map[string][]string
}

var config Config
var ctx context.Context = context.Background()
var client http.Client = http.Client{}

func ReadConfig(path string) bool {
    dat, err := os.ReadFile(path)
    if err != nil {
        log.Fatal("Config path could not be read!")
    }
    err = json.Unmarshal(dat, &config)
    if err != nil {
        log.Fatal("JSON configuration was not in expected format!")
    }
    return true
}

type RPHandle struct {}

func printTimesplit(statusCode int, route string, start *time.Time) {
    end := time.Now()
    fmt.Println(route, statusCode, end.Sub(*start))
}

func (rp *RPHandle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    start := time.Now()
    newReq := r.Clone(ctx)
    newReq.Header.Set("X-Forwarded-For", r.RemoteAddr)

    path := strings.Split(r.URL.Path, "/")
    prefix := "/" + path[1]
    var remainingPath string
    if len(path) > 2 {
        rp, err := url.JoinPath("/", path[2:]...)
        if err != nil {
            http.Error(w, err.Error(), http.StatusBadGateway)
            printTimesplit(http.StatusBadGateway, r.URL.Path, &start)
            return
        }
        remainingPath = rp
    }

    var route string
    var subpath string
    if routes, ok := config.Routes[r.Host]; ok {
        route = routes[rand.Intn(len(routes))]
    } else if routes, ok := config.Routes[prefix]; ok {
        route = routes[rand.Intn(len(routes))]
        subpath = remainingPath
    } else {
        http.Error(w, "Route not found", http.StatusNotFound)
        printTimesplit(http.StatusNotFound, r.URL.Path, &start)
        return
    }

    newURL, err := url.Parse(route)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadGateway)
        printTimesplit(http.StatusBadGateway, r.URL.Path, &start)
        return
    }
    newReq.URL = newURL
    newReq.RequestURI = ""
    newReq.URL.Scheme = "http"

    if subpath != "" {
        newReq.URL.Path = subpath
    }

    resp, err := client.Do(newReq)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadGateway)
        printTimesplit(http.StatusBadGateway, r.URL.Path, &start)
        return
    }
    defer resp.Body.Close()
    for k, vv := range resp.Header {
        for _, v := range vv {
            w.Header().Add(k, v)
        }
    }
    w.WriteHeader(resp.StatusCode)
    io.Copy(w, resp.Body)

    printTimesplit(http.StatusOK, r.URL.Path, &start)
}

func main() {
    if (len(os.Args) < 3) {
        log.Fatal("Need to pass in path and port to config!")
    }
    ReadConfig(os.Args[1])
    fmt.Printf("Listening on port %s...\n", os.Args[2])
    l, err := net.Listen("tcp", ":" + os.Args[2])
	if err != nil {
        log.Fatal(err)
	}
    defer l.Close()

    http.Serve(l, &RPHandle{})
}
